import "./App.css";
import ComposeSalad from "./ComposeSalad";
import { Component } from "react";
import ViewOrder from "./ViewOrder";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      inventory: {},
    };

    this.createOrder = this.createOrder.bind(this);
    this.generateId = this.generateId.bind(this);
    this.fetchIngredient = this.fetchIngredient.bind(this);
    this.buildInventory = this.buildInventory.bind(this);
  }

  static counter = 0;

  createOrder(data) {
    this.generateId(data);
    this.setState({ orders: [...this.state.orders, data] });
  }

  generateId(data) {
    let id = App.counter;
    App.counter++;
    data.getSetId(id);
  }

  async fetchIngredient(type, name) {
    let url = (name === "")
      ? ("http://localhost:8080/".concat(type))
      : ("http://localhost:8080/".concat(type).concat("/" + name));

    await fetch(url)
        .then((response) => response.json())
        .then(function(data) {
          return data;
        })
        .catch((error) => console.log("Fetch error: ", error));
  }

  buildInventory(){

    //foundations
    let type = "foundations";
    fetch("http://localhost:8080/".concat(type))
    .then((response) => response.json())
    .then((data) => (
      data.map((item) =>
        this.setState({ inventory : { type : { item : this.fetchIngredient(type, item) } } }))
    ));

    //Proteins
    type = "proteins";
    fetch("http://localhost:8080/".concat(type))
    .then((response) => response.json())
    .then((data) => (
      data.map((item) =>
        this.setState({ inventory : { type : { item : this.fetchIngredient(type, item) } } }))
    ));

        //extras
    type = "extras";
    fetch("http://localhost:8080/".concat(type))
      .then((response) => response.json())
      .then((data) => (
        data.map((item) =>
          this.setState({ inventory : { type : { item : this.fetchIngredient(type, item) } } }))
    ));

    //dressings
    type = "dressings";
    fetch("http://localhost:8080/".concat(type))
    .then((response) => response.json())
    .then((data) => (
      data.map((item) =>
        this.setState({ inventory : { type : { item : this.fetchIngredient(type, item) } } }))
    ));
  }

  render() {
    if(this.state.inventory != {}){
      this.buildInventory();
    }

    const saladElements = (params) => (
      <ComposeSalad
        {...params}
        inventory={this.state.inventory}
        createOrder={this.createOrder}
      />
    );
    const orderElements = (params) => (
      <ViewOrder {...params}
      order={this.state.orders}
      />
    );

    return (
      <div>
        <Router>
          {
            <div className="jumbotron text-center">
              <h1>Barely Legal Salad Bar</h1>
              <p>Here you can order custom made salads!</p>
            </div>
          }
          {
            <div>
              <ul className="nav nav-pills">
                <li className="nav-item">
                  <Link className="nav-link" to="compose-salad">
                    Compose your own salad
                  </Link>
                  <Link className="nav-link" to="view-order">
                    Show orders
                  </Link>
                </li>
              </ul>
              <ul className="nav nav-pills">
                <li className="nav-item">
                  <Route path="/compose-salad" render={saladElements}></Route>
                  <Route path="/view-order" render={orderElements}></Route>
                </li>
              </ul>
            </div>
          }
        </Router>
      </div>
    );
  }
}

export default App;
